import { Configuration, OpenAIApi } from "openai";
const jwt = require('jsonwebtoken');

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function (req, res) {

  const jwtToken = req.body.jwtToken
  const verify = jwt.verify(jwtToken,process.env.SECRET)
  if(!verify){
    res.status(403).json({
      error: {
        message: "Unauthorized",
      }
    });
    return;
  }
  if (!configuration.apiKey) {
    res.status(500).json({
      error: {
        message: "OpenAI API key not configured, please follow instructions in README.md",
      }
    });
    return;
  }

  try {
    const completion = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: generatePrompt(req.body.name, req.body.age, req.body.gender, req.body.characterClass, req.body.race),
      max_tokens: 200,
    });
    res.status(200).json({ result: completion.data.choices[0].text });
  } catch(error) {
    // Consider adjusting the error handling logic for your use case
    if (error.response) {
      console.error(error.response.status, error.response.data);
      res.status(error.response.status).json(error.response.data);
    } else {
      console.error(`Error with OpenAI API request: ${error.message}`);
      res.status(500).json({
        error: {
          message: 'An error occurred during your request.',
        }
      });
    }
  }
}

export function generatePrompt(name, age, gender, characterClass, race) {
  const capitalizedName = name[0].toUpperCase() + name.slice(1).toLowerCase();
  return `You are a role-playing master, and you want to create a story around a character, this character is called ${capitalizedName}, he is a ${gender} ${race} ${characterClass}, and is ${age} years old. Write his story in 50 words.`;
}
