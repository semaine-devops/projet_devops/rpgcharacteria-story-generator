import { generatePrompt } from "../pages/api/generate";

test('adds 1 + 2 to equal 3', () => {
    const prompt = generatePrompt('anis','25','male','warrior','human')
    expect(prompt).toBe(`You are a role-playing master, and you want to create a story around a character, this character is called Anis, he is a male human warrior, and is 25 years old. Write his story in 50 words.`)
  });